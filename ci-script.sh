#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -euo pipefail

basedir=$(dirname "${BASH_SOURCE[0]}")

undefined_var=0
var_with_space=0
for var in REPO_URL IMAGE_VERSION K8S_VERSION OS_VERSION OS_DEBUG ROLE; do
  if ! [[ -v "${var}" ]]; then
    echo "${var} must be defined"
    undefined_var=1
  fi

  if [[ "${!var}" = *[\ ]* ]]; then 
    echo "${var} contains space character"
    var_with_space=1
 fi
done

if [ "$undefined_var" == 1 ] || [ "$var_with_space" == 1 ]; then
  exit 1
fi

url=${REPO_URL}
if [ -z "${url}" ]; then
  echo "No Nexus repository defined (fail)"
  exit 1
fi

groupId="kanod"
groupPath=$(echo "$groupId" | tr '.' '/')

if [ "${BUILDER_IN_CONTAINER:-0}" == "0" ]; then
  "${PYTHON:-/usr/bin/python3}" -m pip install .
  KANOD_IMAGE_BUILDER="kanod-image-builder"
else
  KANOD_IMAGE_BUILDER="kanod-diskimage-launcher"
fi

export PATH="$HOME/.local/bin:$PATH"

echo "*** Kube elements and images ***"

version=$IMAGE_VERSION
mkdir -p logs

declare -a tpm
if [ "${TPM:-0}" == 1 ]; then
  tpm=(-b tpm)
fi

function build_image() {
  rm -f disk.qcow2
  artifactId=$1
  shift

  # only build if new
  if ! curl --head --silent --output /dev/null --fail "${url}/${groupPath}/${artifactId}/${version}/${artifactId}-${version}.qcow2"; then
    echo
    echo
    echo "************  Building Image ${artifactId} ************"
    echo
    echo "Launching ${KANOD_IMAGE_BUILDER} kanod_node -o disk.qcow2 -s \"k8s_release=${k8s}\" -s \"release=${os}\" ${tpm[*]}" "$@"
    echo
    if [ "${BUILDER_IN_CONTAINER:-0}" == "1" ]; then
      export BUILDER_OS=${target}
      echo "with builder OS : ${BUILDER_OS}"
      echo
    fi
    if ${KANOD_IMAGE_BUILDER} kanod_node -o disk.qcow2 -s "k8s_release=${k8s}" -s "release=${os}" "${tpm[@]}" "$@" &> "logs/${artifactId}.log" && [ -f disk.qcow2 ]; then
      # shellcheck disable=SC2086
      mvn ${MAVEN_CLI_OPTS} -B deploy:deploy-file ${MAVEN_OPTS} \
        -DgroupId="${groupId}" -DartifactId="${artifactId}" \
        -Dversion="${version}" -Dpackaging=qcow2 -Dfile=disk.qcow2 \
        -DrepositoryId=kanod -Durl="${url}" \
        -Dfiles=disk-schema.yaml -Dtypes=yaml -Dclassifiers=schema
      rm -f disk.qcow2 disk-schema.yaml
    else
      echo "Build for ${artifactId} failed"
      cat "logs/${artifactId}.log"
      exit 1
    fi
  else
    echo "artifact ${url}/${groupPath}/${artifactId}/${version}/${artifactId}-${version}.qcow2 already exists"
  fi
}

is_debug_os() {
    case "${1}" in
        [YyTt]* ) return 0;;
        [FfNn]* ) return 1;;
        *) return 1;;
    esac
}

case ${OS_VERSION} in
  centos* )
    target=centos
    os=${OS_VERSION#centos}
    packages="${RPMS:-}"
    ;;
  bionic | focal | jammy )
    target=ubuntu
    os=${OS_VERSION}
    packages="${DEBS:-}"
    ;;
  opensuse* )
    target=opensuse
    os=${OS_VERSION#opensuse}
    packages="${RPMS:-}"
    ;;
  *) echo "Invalid OS_VERSION value : ${OS_VERSION}"
    exit
    ;;
esac

OS_NAME="${OS_VERSION}"

if [ -z "${ROLE}" ]; then 
  ROLE="node"
fi

if [ "${ROLE}" != "node" ]; then
  OS_NAME="${ROLE}"
else
  if is_debug_os "${OS_DEBUG}"; then
    OS_NAME="${OS_NAME}-debug"
  fi
fi

OS_NAME="${OS_NAME}-${K8S_VERSION}"

if [ -n "$version" ]; then
  k8s=${K8S_VERSION}
  if [ "${NO_SYNC_KUBE_ELEMENT:-0}" != "1" ]; then
    echo "Building kube-element ${k8s}"
    "${basedir}/make-kube-element.sh" "${k8s}"
  fi

  if is_debug_os "${OS_DEBUG}"; then
    build_image "${OS_NAME}"  -s target=${target} -s "debug=${SECRET:-secret}" -p "${packages}" -s kanod_role=${ROLE}
  else
    build_image "${OS_NAME}"  -s target=${target} -p "${packages}" -s kanod_role=${ROLE}
  fi
fi
