#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import subprocess
import fnmatch
import json
import sys
import os
from typing import List

from kanod_configure import common


def get_cosign_if_enabled(conf: common.Json):
    '''
    Get the cosign configuration only if it's specified and cosign is enabled.
    '''
    cosign = conf.get('cosign', None)
    if cosign:
        enabled = cosign.get('enabled', False)
        if enabled:
            return cosign
    return None


def is_excepted(exceptions: List[str], candidate: str) -> bool:
    '''
    Verify if an image is excepted from verification, based on an exceptions
    list.
    '''
    for rule in exceptions:
        if fnmatch.fnmatch(candidate, rule):
            return True
    return False


def get_images_to_verify(exceptions: List[str]) -> List[str]:
    '''Get all kubernetes control plane images that need to be verified'''
    kubeadm_command = [
        '/usr/local/bin/kubeadm', 'config', 'images', 'list',
        '-o', 'json', '--config', '/run/kubeadm/kubeadm.yaml']

    proc = subprocess.run(
        kubeadm_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    if proc.returncode != 0:
        print('Failed to extract kubernetes images')
        exit(1)

    data = json.loads(proc.stdout)
    images = data.get('images', None)

    images_to_verify = []

    if images:
        for image in images:
            if not is_excepted(exceptions, image):
                images_to_verify.append(image)
    else:
        print('No images found')
        exit(1)

    return images_to_verify


def get_registries_auth(conf: common.Json):
    '''Get registries that contain credentials'''
    container_registries = conf.get('container_registries', None)

    registries = []

    if container_registries:
        servers = container_registries.get('servers', [])

        for server in servers:
            if server.get('username', None) and server.get('password', None):
                registries.append(server)

    return registries


def cosign_login(conf: common.Json):
    '''Use registry credentials to run cosign login'''
    auths = get_registries_auth(conf)

    for auth in auths:
        command = [
            '/usr/local/bin/cosign', 'login', auth.get('shortname', ''),
            '-u', auth.get('username', ''), '--password-stdin']
        input = auth.get('password', '') + '\n'
        proc = subprocess.run(
            command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
            input=input.encode())
        if proc.returncode != 0:
            print('Failed to login')
            exit(1)


def cosign_logout():
    '''Remove the file created by running cosign login.'''
    os.unlink(os.path.expanduser('~') + '/.docker/config.json')


def cosign_verify_control_plane(arg: common.BootParams):
    '''
    Verify signature of Kubernetes control-plane images at boot.
    The docker credentials need to be configured on the machine before
    running the image verification.
    '''
    print('verify control-plane images with cosign')
    cosign = get_cosign_if_enabled(arg.conf)
    if cosign:
        cosign_login(arg.conf)
        env = os.environ.copy()
        base_command = ['/usr/local/bin/cosign', 'verify']
        cosign_key = cosign.get('key', None)
        if cosign_key:
            env["COSIGN_USER_PUBKEY"] = cosign_key
            base_command.extend(['--key', 'env://COSIGN_USER_PUBKEY'])
        else:
            env["COSIGN_EXPERIMENTAL"] = "1"
            cosign_keyless = cosign.get('keyless', None)
            if cosign_keyless:
                cert_email = cosign_keyless.get('certEmail', '')
                cert_oidc_issuer = cosign_keyless.get('certOIDCIssuer', '')
                base_command.extend([
                    '--certificate-email', cert_email,
                    '--certificate-oidc-issuer', cert_oidc_issuer])

        exceptions = cosign.get('exceptions', [])

        for image in get_images_to_verify(exceptions):
            command = base_command.copy()
            command.append(image)
            proc = subprocess.run(
                command, stdout=sys.stdout, stderr=subprocess.STDOUT, env=env)
            if proc.returncode != 0:
                print('Image verification failed')
                exit(1)
        cosign_logout()


common.register('Signature verification', 100, cosign_verify_control_plane)
