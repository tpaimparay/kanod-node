#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import pathlib
from cloudinit import subp

from . import kubernetes_aux
from . import kanod_containers
from kanod_configure import common

ROOT_REGISTRY = "/etc/containerd/certs.d"

def get_containerd_auth(conf):
    '''get authentication tokens for private registries'''
    containers_vars = conf.get('containers', {})
    raw_auths = containers_vars.get('auths', None)
    if raw_auths is None:
        return None
    auths = [
        {
            "repository": cell.get('repository', ''),
            "username": cell.get('username', ''),
            "password": cell.get('password', ''),
        }
        for cell in raw_auths
    ]
    return auths


def container_engine_containerd_config(conf):
    '''Configure the containerd container engine'''
    proxy_vars = conf.get('proxy')
    if proxy_vars is not None:
        common.render_template(
            'container_engine_proxy.tmpl',
            'etc/systemd/system/containerd.service.d/http-proxy.conf',
            proxy_vars
        )
    registries = conf.get('container_registries', {})
    default_mirrors = registries.get('default_mirrors', None)
    if default_mirrors is not None:
        map = registries.setdefault('map', [])
        map.append({'name': '_default', 'mirrors': default_mirrors})

    common.render_template(
        'containerd_custom_config.tmpl',
        'etc/containerd/config.d/custom-config.toml',
        {'local_pause_container': kubernetes_aux.local_pause_container(conf),
         'reg': registries})
    common.render_template(
        'containerd_kubelet.tmpl',
        'etc/default/kubelet',
        {})
    common.render_template(
        'crictl.tmpl',
        'etc/crictl.yaml',
        {'socket': 'unix:///var/run/containerd/containerd.sock'})

    servers = registries.get('servers', [])
    # Dump the ca hierarchy
    kanod_containers.certificates(servers)

    for registry in registries.get('map', []):
        name = registry.get('name', None)
        if name is None:
            continue
        folder = pathlib.Path(ROOT_REGISTRY, registry['name'])
        folder.mkdir(parents=True, exist_ok=True)
        config = folder.joinpath("hosts.toml")
        server = registry.get('server', None)
        if server is not None:
            server_cfg = kanod_containers.find_registry_server(servers, server)
        else:
            server_cfg = {}
        mirrors = [
            kanod_containers.find_registry_server(servers, mirror)
            for mirror in registry.get('mirrors', [])
        ]
        reg = {
            'mirrors': mirrors
        }
        if server is not None:
            reg['insecure'] = server_cfg.get('insecure', False)
            reg['server'] = server
            reg['shortname'] = server_cfg['shortname']
        reg['ca'] = 'ca' in server_cfg
        reg['client_cert'] = 'client_cert' in server_cfg
        reg['client_key'] = 'client_key' in server_cfg
        common.render_template('hosts_toml.tmpl', config.absolute(), reg)


    subp.subp(['systemctl', 'daemon-reload'])
    subp.subp(['systemctl', 'enable', 'containerd'])
    subp.subp(['systemctl', 'start', 'containerd'])


def register_containerd_engine(arg: common.RunnableParams):
    engines = arg.system.setdefault('container-engines', {})
    engines['containerd'] = container_engine_containerd_config


common.register('Register containerd engine', 50, register_containerd_engine)
