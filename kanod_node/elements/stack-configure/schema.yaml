definitions:
  stack_config_definition:
    type: object
    description: |
      Configuration of the Kanod stack on a life-cycle manager node.
    properties:
      version:
        type: string
        description: version of the Kanod stack
      proxy:
        $ref: '#/definitions/stack_proxy_definition'
      ironic:
        $ref: '#/definitions/ironic_config_definition'
      git:
        $ref: '#/definitions/stack_gits_definition'
      tpm:
        $ref: '#/definitions/tpm_config_definition'
  stack_proxy_definition:
    type: object
    description: Setting for the UI proxy
    required: [ip, certificate, key]
    additionalProperties: false
    properties:
      ip:
        type: string
        description: the external IP
      certificate:
        type: string
        description: x509 certificate chain for the proxy
      key:
        type: string
        description: the certificate associated private key


  ironic_config_definition:
    type: object
    desription: Configuration of Ironic
    additionalProperties: false
    required: [ip, interface, dhcp]
    properties:
      ip:
        type: string
        description: IP shown by the ironic service
      interface:
        type: string
        description: interface on the LCM host used by Ironic for PXE
      ipa_kernel_url:
        type: string
        format: uri
        description: The ironic python agent kernel URI on the Nexus
      ipa_ramdisk_url:
        type: string
        format: uri
        description: The ironic python agent ramdisk URI on the Nexus
      dhcp:
        $ref: '#/definitions/ironic_dhcp_config_definition'


  ironic_dhcp_config_definition:
    type: object
    additionalProperties: false
    properties:
      ironic_ip:
        type: string
        description: ironic ip address seen from PXE network
      ironic_port:
        type: integer
        description: port for tftp
      subnets:
        type: array
        items:
          $ref: '#/definitions/ironic_dhcp_subnet_definition'
      hosts:
        type: array
        description: list of hosts handled by this DHCP
        items:
          $ref: '#/definitions/ironic_dhcp_host_definition'

  ironic_dhcp_subnet_definition:
    type: object
    properties:
      prefix:
        description: subnetwork prefix
        type: string
      mask:
        description: subnetwork mask
        type: string
      start:
        description: start of range for addresses allocated
        type: string
      end:
        description: end of range for addresses allocated
        type: string

  ironic_dhcp_host_definition:
    type: object
    properties:
      name:
        description: arbitrary name
        type: string
      mac:
        description: mac address
        type: string
      ip:
        description: forced ip (optional)
        type: string


  stack_gits_definition:
    type: object
    description: Configuration of git repositories for gitops
    additionalProperties: false
    properties:
      baremetal:
        $ref: '#/definitions/stack_git_config_definition'
      projects:
        $ref: '#/definitions/stack_git_config_definition'
      certificates:
        type: object
        additionalProperties:
          type: string
        description: dictionary of certificates for git repos

  stack_git_config_definition:
    type: object
    required: [url]
    properties:
      url:
        type: string
        format: uri
        description: URL of the repository
      username:
        type: string
        description: user to access the repository
      password:
        type: string
        description: password to access the repository

  tpm_config_definition:
    type: object
    required: [auth_url]
    additionalProperties: false
    definition: parameters for authentication of machines through their TPM
    properties:
      version:
        type: string
        description: version of the registrar used by the stack
      auth_url:
        type: string
        description: url of the endpoint for authentication
      auth_ca:
        type: string
        description: CA certificate for the endpoint.
properties:
  stack:
    $ref: '#/definitions/stack_config_definition'
